Markdown
=========

Role that installs Markdown

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Docker on targeted machine:

    - hosts: servers
      roles:
         - markdown

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
